FROM docker.io/nimmis/alpine:3.14

# Install Apache
RUN apk update \
    && apk upgrade \
    && apk add apache2 apache2-brotli apache2-proxy apache2-http2 apache2-ssl \
    && mkdir -p /var/www/public \
    && rm -rf /var/www/localhost \
    && chown -R apache:apache /var/www \
    && rm -rf /var/cache/apk/*

# Install PHP-FPM
RUN apk update \
    && apk upgrade \
    && apk add curl \
        php7-bcmath \
        php7-brotli \
        php7-bz2 \
        php7-curl \
        php7-ffi \
        php7-fileinfo \
        php7-fpm \
        php7-gd \
        php7-gettext \
        php7-gmp \
        php7-iconv \
        php7-json \
        php7-mbstring \
        php7-mysqli \
        php7-opcache \
        php7-openssl \
        php7-pdo \
        php7-pdo_mysql \
        php7-pdo_sqlite \
        php7-pecl-apcu \
        php7-pecl-imagick \
        php7-pecl-mcrypt \
        php7-phar \
        php7-posix \
        php7-session \
        php7-shmop \
        php7-simplexml \
        php7-soap \
        php7-sodium \
        php7-sysvmsg \
        php7-sysvsem \
        php7-sysvshm \
        php7-xml \
        php7-zip \
    && rm -rf /var/cache/apk/*

# Install SMMTP agent
RUN apk update \
    && apk upgrade \
    && apk add msmtp \
    && rm -rf /var/cache/apk/*

# Copy our root filesystem
COPY ./rootfs/ /

# expose our http and https ports
EXPOSE 80 443
