#!/bin/sh
set -e

export SMTP_HOST=${SMTP_HOST:-}
export SMTP_PORT=${SMTP_PORT:-}
export SMTP_USER=${SMTP_USER:-}
export SMTP_PASSWORD=${SMTP_PASSWORD:-}
export SMTP_FROM=${SMTP_FROM:-}

# Create the default config file.
cat <<EOF >/etc/msmtprc
# Set defaults.
defaults

# Enable or disable TLS/SSL encryption.
tls on
tls_starttls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt

# Set up a default account's settings.
account default
host ${SMTP_HOST}
port ${SMTP_PORT}
auth on
user ${SMTP_USER}
password ${SMTP_PASSWORD}
from ${SMTP_FROM}
syslog LOG_MAIL
EOF
