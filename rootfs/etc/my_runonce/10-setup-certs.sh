#!/bin/sh
#
# Ensure we have the TLS certificates installed
# If there are no certificates installed we will created a self-signed one.
#

set -e

directory=/etc/ssl/apache2

# the files used by apache2
key_file="${directory}/server.key"
cert_file="${directory}/server.pem"
chain_file="${directory}/server-chain.pem"

# check if the files already exist
if [ ! -f "${key_file}" ] || [ ! -f "${cert_file}" ]; then
    # create a new self signed certificate if it isn't installed
    openssl req -x509 \
        -newkey rsa:4096 \
        -nodes \
        -keyout "${key_file}" \
        -out "${cert_file}" \
        -days 365 \
        -subj "/CN=${SERVER_NAME}" \
        -addext "subjectAltName = DNS.0:${SERVER_NAME},DNS.1:${SERVER_ALIAS},IP.0:$(hostname -i)"

    # just making sure the file permissions are correct
    chmod 600 "${key_file}"
    chmod 644 "${cert_file}"
fi

if [ ! -f "${chain_file}" ]; then
    cp "${cert_file}" "${chain_file}"
fi

cp "${chain_file}" /etc/ssl/certs/

# update our CA trust
update-ca-certificates --fresh
# The above line makes sure that our container trusts our certificate
# This is helpful when using custom certificates like those from Cloudflare or self signed
