# docker-apache-php-fpm-alpine

This is a basic image which run Apache and PHP-FPM. It was originally designed to run [PHPTRAVELS](https://phptravels.com), however it should be able to run any PHP project.

This image was build on the [nimmis/docker-alpine image](https://github.com/nimmis/docker-alpine), a thanks to [Kjell Havnesköld](https://github.com/nimmis) for the work done on that.

## Pulling Image

This image is hosted using GitLab's container registry, so you can pull the images using the following command:

```shell
docker pull registry.gitlab.com/jlisher/docker-apache-php-fpm-alpine:latest
```

## Volume Mount Points

This image has a simple filesystem that you can use to add your application and TLS certificates to.

Your application can be installed in the `/var/www/public` directory, which  will serve as the document root.

You can also add your own TLS certificates in the `/etc/ssl/apache2` directory. The exact files you will need to add are: 
- `/etc/ssl/apache2/server.key` for the TLS key, 
- `/etc/ssl/apache2/server.pem` for the TLS certificate, 
- and optionally `/etc/ssl/apache2/server-chain.pem` for the full TLS chain including the CA certificate.

> Note that a default TLS certificate will be generated if one doesn't exist.

## Environment Variables

There are a few environment variables that can be used to allow for sending emails using PHP's built-in sendmail support.
The following variables are available:

| Variable | Description | Value |
| --- | --- | --- |
| SMTP_HOST | SMTP sever hostname | Not Set |
| SMTP_PORT | SMTP sever port | Not Set |
| SMTP_USER | SMTP sever username | Not Set |
| SMTP_PASSWORD | SMTP sever password | Not Set |
| SMTP_FROM | the email address to send from by default | Not Set |

## Contributions

Please feel free create new issues and PRs for any updates you would like to see. All contributions welcome.
